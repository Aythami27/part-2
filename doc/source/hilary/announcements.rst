`Back to Numerics Course Home Page <http://mpecdt.bitbucket.org/>`_

Copy of the Announcements made on Blackboard
=============================================================

* Friday 9 Oct 2015

  The videos for week 2 of the numerics course are now available at:

  http://mpecdt.bitbucket.org/

  This material will enable you to do questions 1-4 of assignment 2. The material on dispersion for questions 5 will be covered in week 3. Watch the videos before the lecture time on Tuesday 13 October so that you can ask questions on the material, attempt the exercises and work on the assignment during the lecture time. There will not be a numerics tutorial on Wednesday during week 2.

  Remember to post questions to the blackboard discussion forum
  
  https://www.bb.reading.ac.uk

* Tuesday 6 Oct 2015

  On request, I am giving you a digital copy of the source code that was in the pdf file describing the assignment. I have committed the changes to the bitbucket repository. So, if you want to use the new files and you have already made some of your own edits, you will need to merge the two repositories. See git documentation. Alternatively you could just sync your forked repository, or stick with what you are doing.

  This is an announcement on Reading blackboard which should get sent to everybody's Reading email addresses. Please forward to anyone who you suspect may not be able to access their Reading email, and remind them to put a re-direct on their Reading email so that they will always receive important announcements.


